// Copyright (C) 2009 - 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Representable numbers

// Smallest Positive Normal : 2.22D-308 
2^-1022

// Largest Positive Normal : 1.79D+308
(2-2^(1-53))*2^1023

// Smallest Positive Denormal : 4.94D-324
2^(-1022-53+1)

// Machine precision : 2.220D-16
2^(1-53)

// demo of nan

%nan == %nan
isnan(%nan)

// demo of the two zeros
-0 == +0
gamma(-0)
gamma(+0)
ieee(2)
1/-0
1/+0
ieee(0)

// demo of inf
%inf + %inf
%inf - %inf
2 * %inf
%inf / 2 
1 / %inf

