// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Spacing between floating point numbers
radix = 2
p = 3
emin = -2
emax = 3
//
Mmin = radix^(p - 1)
Mmax = radix^p - 1
//
f = [];
for e = emax : -1 : emin
  for M = -Mmax : -Mmin
    f($+1) = M * radix^(e - p + 1);
  end
end
f($+1) = 0;
for e = emin : emax
  for M = Mmin : Mmax
    f($+1) = M * radix^(e - p + 1);
  end
end

// With denormals
f = [];
for e = emax : -1 : emin
  for M = -Mmax : -Mmin
    f($+1) = M * radix^(e - p + 1);
  end
end
e = emin;
for M = -Mmin + 1 : -1
  f($+1) = M * radix^(e - p + 1);
end
f($+1) = 0;
e = emin;
for M = 1 : Mmin - 1
  f($+1) = M * radix^(e - p + 1);
end
for e = emin : emax
  for M = Mmin : Mmax
    f($+1) = M * radix^(e - p + 1);
  end
end

// Float gui
r = 2;
p = 3;
e = 3;
flps = flps_format2system ( r , p , e );

// Compute the machine epsilon for this system
flps = flps_format2system ( 2 , 3 , 3 );
flps_systemprint ( flps );
flpn = flps_numberformat ( flps , 1 );
flps_numberprint(flps,flpn);
flpn = flps_numberformat ( flps , 1.25 );
flps_numberprint(flps,flpn);


// What is the distance between x and x- when e- = e - 1
flps = flps_format2system ( 2 , 3 , 3 );
flps_systemprint ( flps );
// x = 1
flpn = flps_numberformat ( flps , 1 );
flps_numberprint(flps,flpn);
// x- = 0.875
flpn = flps_numberformat ( flps , 0.875 );
flps_numberprint(flps,flpn);


