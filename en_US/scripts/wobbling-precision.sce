// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Show wobbling precision in round to nearest

flps = flps_systemnew ( "format", 2 , 3 , 3 );

// Show only positive normals
r = [];
n = 1000;
xv = linspace ( 0.25 , 14 , n );
for i = 1 : n
  x = xv(i);
  flpn = flps_numbernew("double",flps,x);
  f = flps_numbereval ( flpn );
  r(i) = abs(f-x)/abs(x);
end
plot ( xv , r )


// Show only positive subnormals
r = [];
n = 1000;
xv = linspace ( 0.01 , 0.25 , n );
for i = 1 : n
  x = xv(i);
  flpn = flps_numbernew("double",flps,x);
  f = flps_numbereval ( flpn );
  r(i) = abs(f-x)/abs(x);
end
plot ( xv , r )
  
// Show all numbers
r = []
n = 1000;
xv = linspace ( -14 , 14 , n );
for i = 1 : n
  x = xv(i);
  flpn = flps_numbernew("double",flps,x);
  f = flps_numbereval ( flpn );
  r(i) = abs(f-x)/abs(x);
end
plot ( xv , r )
  

// Show wobbling precision in round up
flps = flps_format2system ( 2 , 3 , 3 )
flps.r = 2

// Show only positive normals
r = []
n = 1000;
xv = linspace ( 0.25 , 14 , n );
for i = 1 : n
  x = xv(i);
  flpn = flps_numbernew("double",flps,x);
  f = flps_numbereval ( flpn );
  r(i) = abs(f-x)/abs(x);
end
plot ( xv , r )

// Show all numbers
r = []
n = 1000;
xv = linspace ( -14 , 14 , n );
for i = 1 : n
  x = xv(i);
  flpn = flps_numbernew("double",flps,x);
  f = flps_numbereval ( flpn );
  r(i) = abs(f-x)/abs(x);
end
plot ( xv , r )



// Show wobbling precision in round down
flps = flps_format2system ( 2 , 3 , 3 )
flps.r = 3

// Show only positive normals
r = []
n = 1000;
xv = linspace ( 0.25 , 14 , n );
for i = 1 : n
  x = xv(i);
  flpn = flps_numbernew("double",flps,x);
  f = flps_numbereval ( flpn );
  r(i) = abs(f-x)/abs(x);
end
plot ( xv , r )

// Show all numbers
r = []
n = 1000;
xv = linspace ( -14 , 14 , n );
for i = 1 : n
  x = xv(i);
  flpn = flps_numbernew("double",flps,x);
  f = flps_numbereval ( flpn );
  r(i) = abs(f-x)/abs(x);
end
plot ( xv , r )



// Show wobbling precision in round to zero
flps = flps_format2system ( 2 , 3 , 3 )
flps.r = 4

// Show only positive normals
r = []
n = 1000;
xv = linspace ( 0.25 , 14 , n );
for i = 1 : n
  x = xv(i);
  flpn = flps_numbernew("double",flps,x);
  f = flps_numbereval ( flpn );
  r(i) = abs(f-x)/abs(x);
end
plot ( xv , r )

// Show all numbers
r = []
n = 1000;
xv = linspace ( -14 , 14 , n );
for i = 1 : n
  x = xv(i);
  flpn = flps_numbernew("double",flps,x);
  f = flps_numbereval ( flpn );
  r(i) = abs(f-x)/abs(x);
end
plot ( xv , r )




