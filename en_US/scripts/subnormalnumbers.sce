// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

flps = flps_systemnew ( "format", 2 , 5 , 3 );
ebits = flps.ebits;
p = flps.p;
listflpn = flps_systemall ( flps );
n = size(listflpn);
for i = 1:  n
  flpn = listflpn(i);
  issub = flps_numberissubnormal ( flpn );
  iszer = flps_numberiszero ( flpn )
  if ( iszer ) then
    impbit = "   ";
  else 
    if ( issub ) then
      impbit="(0)";
    else
      impbit="(1)";
    end
  end
  [hexstr,binstr] = flps_number2hex(flpn);
  f = flps_numbereval ( flpn );
  sign_str = part(binstr,1);
  expo_str = part(binstr,2:ebits+1);
  M_str = part(binstr,ebits+2:ebits+p);
  mprintf("%3d: v=%f, e=%2d, M=%s%s\n",i,f,flpn.e,impbit,M_str);
end


