// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// A wrong implementation, with cumulated 
// errors.
function c = mycolon_naive ( a , s , b )
  x = a
  i = 1
  while ( x <= b )
    c(i) = x
    x = x + s
    i = i + 1
  end
endfunction

format(25)  
// 0:0.1:1 > should be 11 values
size(mycolon_naive ( 0 , 0.1 , 1 ))
mycolon_naive ( 0 , 0.1 , 1 )
// 0:0.01:1 > should be 101 values
size(mycolon_naive ( 0 , 0.01 , 1 ))
mycolon_naive ( 0 , 0.01 , 1 )
// Conclusion, one value is missing, 
// because the test ( x <= b ) is false 
// one iteration earlier than required.

// A wrong implementation, with cumulated 
// errors, again.
function c = mycolon_naive2 ( a , s , b )
  x = a
  n = floor ( (b-a)/s ) + 1
  for i = 1 : n
    c(i) = x
    x = x + s
  end
endfunction

format(25)  
// 0:0.1:1 > should be 11 values
size(mycolon_naive2 ( 0 , 0.1 , 1 ))
mycolon_naive2 ( 0 , 0.1 , 1 )
// 0:0.01:1 > should be 101 values
size(mycolon_naive2 ( 0 , 0.01 , 1 ))
c = mycolon_naive2 ( 0 , 0.01 , 1 )
mprintf("%e\n", abs(c(101)-1))
-log10(abs(c(101)-1))
-log10(%eps)
// Conclusion:
// All the values are there, but the 
// last value is very inaccurate:
// we have lost ~0.5 decimal digit.

// Bad implementation for the last 
// term
function x = mycolon_last ( a , s , b )
  x = a
  n = floor ( (b-a)/s ) + 1
  i = 1
  while ( i < n )
    x = x + s
    i = i + 1 
  end
endfunction

for s = [0.1 0.01 0.001 1.e-3 1.e-4 1.e-6 1.e-8]
  c = mycolon_last ( 0 , s , 1 );
  mprintf("s=%e, e = %e\n", s , abs(c-1))
end

//s=1.000000e-001, e = 1.110223e-016
//s=1.000000e-002, e = 6.661338e-016
//s=1.000000e-003, e = 6.661338e-016
//s=1.000000e-003, e = 6.661338e-016
//s=1.000000e-004, e = 9.381385e-014
//s=1.000000e-006, e = 7.918111e-012
//s=1.000000e-008, e = 2.289867e-009

// A good implementation.
function c = mycolon ( a , s , b )
  n = floor ( (b-a)/s ) + 1
  for i = 1 : n
    c(i) = a + s * (i-1)
  end
endfunction

format(25)  
// 0:0.1:1 > should be 11 values
size(mycolon ( 0 , 0.1 , 1 ))
mycolon_naive2 ( 0 , 0.1 , 1 )
// 0:0.01:1 > should be 101 values
size(mycolon ( 0 , 0.01 , 1 ))
c = mycolon ( 0 , 0.01 , 1 )
abs(c(101)-1)
// Conclusion:
// All the values are there, and 
// the last value is exact.


