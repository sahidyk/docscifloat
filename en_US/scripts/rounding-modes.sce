// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Some rounding examples
flps = flps_format2system ( 2 , 3 , 3 )

// See the representation of 0.5 and 0.625

// 0.5 : M=4, e=-1
flpn = flps_numberformat ( flps , 0.5 )

// 0.625 : M=5, e=-1
flpn = flps_numberformat ( flps , 0.625 )

// See that 0.54 is rounded to 0.5 in round to nearest mode
x = 0.54;
x1 = 0.5;
x2 = 0.625;
abs(x-x1)
abs(x-x2)

flpn = flps_numberformat ( flps , 0.54 )

// See that 0.60 is rounded to 0.625 in round to nearest mode
x = 0.60;
x1 = 0.5;
x2 = 0.625;
abs(x-x1)
abs(x-x2)

flpn = flps_numberformat ( flps , 0.60 )

// A case of tie in round to nearest
x= 4.5 * 2^(-1-3+1)
x1 = 0.5;
x2 = 0.625;
abs(x-x1)
abs(x-x2)

flpn = flps_numberformat ( flps , x )



