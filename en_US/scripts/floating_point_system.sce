// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// A floating point system
radix = 2
p = 53
emin = -1022
emax = 1023

// One floating point numbers exactly representing a real number
flps = flps_format2system ( 2 , 3 , 3 );
flps_systemprint ( flps );
flpn = flps_numberformat ( flps , 4 );
flps_numberprint(flps,flpn);

// Two floating point numbers exactly representing one real number
flps = flps_format2system ( 2 , 3 , 3 );
flps_systemprint ( flps );
flpn = flps_numberformat ( flps , 3 );
flps_numberprint(flps,flpn);

// One floating point numbers not exactly representing a real number
flps = flps_format2system ( 2 , 3 , 3 );
flps_systemprint ( flps );
flpn = flps_numberformat ( flps , 4.5 );
flps_numberprint(flps,flpn);
f = flps_numbereval ( flps , flpn )

// The maximum integral significant for this system
// 9 007 199 254 740 991
format(25)
Mmax = radix^p - 1

// See all floating point numbers with denormals
flps = flps_format2system ( 2 , 3 , 3 );
flpn = flps_systemall ( flps );
f = flps_numbereval ( flps , flpn )

// See all floating point numbers without denormals
flps = flps_format2system ( 2 , 3 , 3 );
denormals = %f;
flpn = flps_systemall ( flps , denormals );
f = flps_numbereval ( flps , flpn )

// A denormalized
flps = flps_format2system ( 2 , 3 , 3 );
flpn = flps_numberformat ( flps , 0.125 );
flps_numberprint(flps,flpn);

// Plot all floating point numbers
flps = flps_format2system ( 2 , 3 , 3 );
listflpn = flps_systemall ( flps );
n = size(listflpn);
// Evaluate all numbers
for i = 1 : n
  f = flps_numbereval ( flps , listflpn(i) );
  //disp("========================")
  //disp([i f])
  plot(f,0,"bo")
  //flps_numberprint ( flps , listflpn(i) );
end


