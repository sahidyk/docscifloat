changelog of the Documentation : Floating Point Numbers in Scilab

docscifloat  (0.4)
 * Added a note on temporary calls to the ieee function.

docscifloat  (0.3)
 * Fixed the "implicit bit" section.
 * Fixed the exercises.
 * Clarified exercise on sin(pi).
 * Added exercises for section 1.
 * Added exercise for subnormal numbers.

 docscifloat  (v0.2)
 * Clarified "Properties of arithmetic".
 * Added an exercise on rounding 0.1.
 * Clarified description of Nan.
 * Fixed bugs in the subnormal numbers.
 * Fixed typos.
 * Software-aided spell checking.
 * Fixed bibliography.

docscifloat  (v0.1)
 * First public release of draft: November 2011
 * Added examples in the "Scilab" section.
 * Added a table of related functions.
 * Added comments in the Scilab section.
 * Partly filled section for Inf, Nan and ieee.
 * Added exercises.


